<?php
namespace Albelli\Core;

class Renderer {

    public function sendOutput($response)
    {
        $templateFile = ROOTPATH . DIRECTORY_SEPARATOR
            . 'templates' . DIRECTORY_SEPARATOR
            . $response['template'] . '.phtml';

        foreach ($response['data'] as $key => $value)
        {
            ${$key} = $value;
        }

        ob_start();
        include $templateFile;
        $content = ob_get_contents();
        ob_end_clean();

        include ROOTPATH . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'layout/default.phtml';
    }

}
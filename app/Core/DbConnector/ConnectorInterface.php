<?php
namespace Albelli\Core\DbConnector;


interface ConnectorInterface {

    /**
     * Add new row
     * @param array $data
     * @return boolean
     */
    public function save(array $data);

    /**
     * Update data
     * @param array $data
     * @return boolean
     */
    public function update(array $data);

    /**
     * @return array
     */
    public function getList();

    /**
     * @param string $tableName
     * @return null
     */
    public function setTable($tableName);

}
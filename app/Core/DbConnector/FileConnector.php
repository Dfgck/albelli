<?php
namespace Albelli\Core\DbConnector;


class FileConnector implements ConnectorInterface {

    const DB_FILES_PATH = 'Db';
    /**
     * @var string
     */
    protected $table = '';

    protected $filePath = '';
    /**
     * @param array $data
     * @return boolean
     */
    public function save(array $data)
    {
        if (empty($this->table)) {
            throw new \Exception('Table name not set!');
        }

        $tableData = $this->getFileData();
        $tableData[] = $data;

        $handler = $this->getFileHandler('w');
        fwrite($handler, json_encode($tableData));
        fclose($handler);

        return true;
    }

    public function update(array $data)
    {
        if (empty($this->table)) {
            throw new \Exception('Table name not set!');
        }

        $handler = $this->getFileHandler('w');
        fwrite($handler, json_encode($data));
        fclose($handler);

        return true;
    }

    /**
     * @return array
     */
    public function getList()
    {
        return $this->getFileData();
    }

    /**
     * @param string $tableName
     * @return null
     */
    public function setTable($tableName)
    {
        $this->table = $tableName;

        if (!is_writable($this->getStoragePath())) {
            throw new \Exception("{$this->getStoragePath()} must be writable");
        }

        if (!file_exists($this->getFilePath())) {
            fclose($this->getFileHandler('w'));
        }
    }

    protected function getFilePath()
    {
        if (empty($this->filePath)) {
            $this->filePath = $this->getStoragePath() . $this->table . '.json';
        }

        return $this->filePath;
    }

    protected function getStoragePath()
    {
        $documentRoot = isset($_SERVER['DOCUMENT_ROOT']) ? $_SERVER['DOCUMENT_ROOT'] : ROOTPATH;

        return $documentRoot . DIRECTORY_SEPARATOR
        . static::DB_FILES_PATH . DIRECTORY_SEPARATOR;
    }

    protected function getFileHandler($mode = 'r')
    {
        return fopen($this->getFilePath(), $mode);
    }

    protected function getFileData()
    {
        $handler = $this->getFileHandler();
        $fileSize = filesize($this->getFilePath());
        $data = $fileSize > 0
            ? json_decode(fread($handler, filesize($this->getFilePath())), true)
            : [];
        fclose($handler);

        return $data;
    }
}
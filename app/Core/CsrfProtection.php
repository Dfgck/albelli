<?php
namespace Albelli\Core;

class CsrfProtection {
    const CSRF_PROTECTION_KEY_NAME = 'csrf_protection';

    public function setCsrf()
    {
        $_SESSION[static::CSRF_PROTECTION_KEY_NAME] = md5(time() . 'Lorem Ipsum');
    }

    public function checkCsrf($csrf)
    {
        return $_SESSION[static::CSRF_PROTECTION_KEY_NAME] == $csrf;
    }

}
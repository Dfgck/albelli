<?php
namespace Albelli\Core;

interface ModelInterface {

    /**
     * @return array
     */
    public function getList();

    /**
     * @param array $data
     * @return boolean
     */
    public function save(array $data);

    /**
     * @param $tableName
     * @return null
     */
    public function setTable($tableName);
}
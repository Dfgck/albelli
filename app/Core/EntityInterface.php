<?php
namespace Albelli\Core;

interface EntityInterface {

    /**
     * @return string
     */
    public function get($name);

    /**
     * @param string $name
     * @param mixed $value
     * @return null
     */
    public function set($name, $value);

}
<?php
namespace Albelli\Core;

use Albelli\Core\DbConnector\ConnectorInterface;
use Albelli\Core\DbConnector\FileConnector;
use Albelli\Model\Validator\ValidatorInterface;

abstract class AbstractModel implements ModelInterface {

    /**
     * @var ModelInterface
     */
    protected $model;

    /**
     * @var ConnectorInterface
     */
    protected $dbConnector;

    /**
     * @var string
     */
    protected $table;

    /**
     * @var ValidatorInterface[]
     */
    protected $validators = [];

    public function __construct(ConnectorInterface $dbConnector = null)
    {
        $this->dbConnector = !is_null($dbConnector) ? $dbConnector : new FileConnector();
    }

    public function setTable($tableName)
    {
        $this->dbConnector->setTable($tableName);
    }

    public function validate($data)
    {
        foreach ($this->validators as $validator)
        {
            $validator->check($data);
        }
    }

    public function addValidator(ValidatorInterface $validator)
    {
        $this->validators[] = $validator;
    }
}
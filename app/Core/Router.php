<?php
/**
 * Created by PhpStorm.
 * User: dfg
 * Date: 17.02.2018
 */

namespace Albelli\Core;


class Router {

    /**
     * @var array
     */
    protected $params;

    protected $request = [
        'prefix' => 'Index',
        'controller' => 'Albelli\Controller\Index\Index',
        'request' => []
    ];

    public function __construct(array $params)
    {
        $this->params = $params;

        if (!empty($_POST)) {
            $this->request['request']['data'] = $_POST;
        }

        if (!empty($_FILES)) {
            $this->request['request']['data'] += $_FILES;
        }
    }

    public function getRequest()
    {
        if (empty($this->params)) {
            return $this->request;
        }

        $this->parthRoute();

        return $this->request;
    }

    protected function parthRoute()
    {
        $path = explode('/', $this->params['route']);
        switch (sizeof($path)) {
            case 2 :
                $this->request['prefix'] = ucfirst($path[0]);
                $this->request['controller'] = 'Albelli\\Controller\\' . $this->request['prefix'] . '\\' . ucfirst($path[1]);
                break;
            case 1 :
                $this->request['prefix'] = $path[0];
                $this->request['controller'] = 'Albelli\\Controller\\' . $this->request['prefix'] . '\\Index';
                break;
        }
    }

}
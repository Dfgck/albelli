<?php
namespace Albelli\Core\File;


abstract class AbstractFile {
    /**
     * @var string
     */
    protected $absFilePath;
    /**
     * @var string
     */
    protected $fileType;
    /**
     * @var string
     */
    protected $relPath;

    public function __construct($relPath)
    {
        $this->relPath = $relPath;
    }


    public function getAbsPath()
    {
        if (!$this->absFilePath) {
            $this->absFilePath = ROOTPATH . DIRECTORY_SEPARATOR . $this->relPath;
        }

        return $this->absFilePath;
    }

    public function getFileType($fileName)
    {
        if (!$this->fileType) {
            $this->fileType = strtolower(pathinfo($fileName,PATHINFO_EXTENSION));
        }

        return $this->fileType;
    }

}
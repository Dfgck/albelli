<?php
namespace Albelli\Core\File;

use Albelli\Core\Exception\UserException;

class Upload extends AbstractFile {
    /**
     * @var array
     */
    protected $availableTypes;
    /**
     * @var array
     */
    protected $file;

    public function __construct($availableTypes = [], $relPath)
    {
        $this->availableTypes = $availableTypes;
        parent::__construct($relPath);
    }

    public function upload($file)
    {
        if (!is_writable($this->getAbsPath())) {
            throw new \Exception("{$this->getStoragePath()} must be writable");
        }

        $this->checkType($file);
        $fileName = time() . '.' . $this->getFileType($file['name']);

        if (!move_uploaded_file($file['tmp_name'], $this->getAbsPath() . DIRECTORY_SEPARATOR . $fileName)) {
            throw new UserException('Unable to upload file');
        }

        return [
            'name' => $fileName,
            'abs_path' => $this->getAbsPath() . DIRECTORY_SEPARATOR . $fileName,
            'rel_path' => $this->relPath . DIRECTORY_SEPARATOR . $fileName,
            'file_type' => $this->getFileType($file['name']),
        ];
    }

    protected function checkType($file)
    {
        $fileType = $this->getFileType($file['name']);

        if (!in_array($fileType, $this->availableTypes)) {
            throw new UserException("Unable to upload '$fileType' type file");
        }
    }


}
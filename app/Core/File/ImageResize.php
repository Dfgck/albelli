<?php
/**
 * Created by PhpStorm.
 * User: dfg
 * Date: 17.02.2018
 */

namespace Albelli\Core\File;


class ImageResize {

    /**
     * @var string
     */
    protected $absPath;

    public function __construct($absPath)
    {
        $this->absPath = $absPath;
    }

    /**
     * Smelly image resize
     *
     * @param $type
     * @param $newWidth
     * @param $newHeight
     *
     * @TODO Functionality for correct image resizing
     */
    public function resize($type, $newWidth, $newHeight)
    {
        list($currentWidth, $currentHeight) = getimagesize($this->absPath);

        $image = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled(
            $image,
            $this->createFrom($type, $this->absPath),
            0,
            0,
            0,
            0,
            $newHeight,
            $newHeight,
            $currentWidth,
            $currentHeight
        );

        imagejpeg($image, $this->absPath);
    }

    protected function createFrom($type, $file)
    {
        switch ($type) {
            case 'jpg':
                $image = imagecreatefromjpeg($file);
                break;
            case 'png':
                $image = imagecreatefrompng($file);
                break;
            default:
                throw new \Exception("Undefined type '$type'");
        }

        return $image;
    }
}
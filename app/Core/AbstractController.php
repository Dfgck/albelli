<?php
namespace Albelli\Core;


abstract class AbstractController {

    /**
     * @var ModelInterface
     */
    protected $model;

    protected $response = [
        'data' => [],
        'headers' => [],
        'template' => [],
    ];
    /**
     * @var array
     */
    protected $request;

    public function __construct(array $request)
    {
        $this->request = $request;
    }

    protected function addToResponse($key, $data)
    {
        $this->response['data'][$key] = $data;
    }

    protected function setTemplate($name)
    {
        $this->response['template'] = $name;
    }

    protected function redirect($url) {
        header('location:' . $url);
        exit();
    }

    abstract public function execute();
}
<?php
namespace Albelli\Controller\Posts;

use Albelli\Core\AbstractController;
use Albelli\Core\Exception\UserException;
use Albelli\Core\File\ImageResize;
use Albelli\Core\File\Upload;
use Albelli\Model\Post;
use Albelli\Model\Validator\AdminEmailValidator;
use Albelli\Model\Validator\EmailValidator;
use Albelli\Model\WordsCounter;

class Save extends AbstractController {

    /**
     * @var Upload
     */
    protected $fileUploader;

    /**
     * @var WordsCounter
     */
    protected $wordsCounter;

    public function __construct(array $request)
    {
        $this->model = new Post();
        $this->model->setTable('posts');
        $this->model->addValidator(new AdminEmailValidator());
        $this->model->addValidator(new EmailValidator());

        $this->wordsCounter = new WordsCounter();
        $this->wordsCounter->setTable('words_counter');

        $this->fileUploader = new Upload(['png', 'jpg'], 'pub' . DIRECTORY_SEPARATOR . 'images');

        parent::__construct($request);
    }

    public function execute()
    {

        if (empty($this->request['data'])) {
            throw new UserException('Unable to save empty post');
        }

        if (isset($this->request['data']['post_image']['error'])
            && $this->request['data']['post_image']['error'] === 0
        ) {
            $file = $this->fileUploader->upload(
                $this->request['data']['post_image']
            );

            (new ImageResize($file['abs_path']))->resize($file['file_type'],300, 300);
        }

        $this->request['data']['post_image'] = isset($file['rel_path']) ? $file['rel_path'] : '';
        $this->request['data']['post_timestamp'] = time();
        $this->model->save($this->request['data']);

        $this->wordsCounter->save($this->request['data']);

        if ($this->request['data']['method'] != 'ajax') {
            $this->redirect('/');
        } else {
            exit(0);
        }
    }
}
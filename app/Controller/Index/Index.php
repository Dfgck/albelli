<?php
namespace Albelli\Controller\Index;

use Albelli\Core\AbstractController;
use Albelli\Model\Post;
use Albelli\Model\WordsCounter;

class Index extends AbstractController {

    /**
     * @var WordsCounter
     */
    protected $wordsCounter;

    public function __construct($request)
    {
        $this->model = new Post();
        $this->model->setTable('posts');

        $this->wordsCounter = new WordsCounter();
        $this->wordsCounter->setTable('words_counter');

        $this->setTemplate('posts');

        parent::__construct($request);
    }

    public function execute()
    {
        $this->addToResponse('posts', $this->model->getList());
        $this->addToResponse('words', array_slice($this->wordsCounter->getList(), 0, 5));

        return $this->response;
    }

}
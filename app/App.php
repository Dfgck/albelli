<?php
namespace Albelli;

use Albelli\Core\CsrfProtection;
use Albelli\Core\Exception\UserException;
use Albelli\Core\Renderer;
use Albelli\Core\Router;

class App {

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var Renderer
     */
    protected $renderer;

    /**
     * @var CsrfProtection
     */
    protected $csrf;

    public function __construct()
    {
        $this->csrf = new CsrfProtection();
    }

    public function execute()
    {
        $this->dispatch($this->getRouter()->getRequest());
    }

    protected function dispatch(array $request)
    {
        try {
            if (isset($request['request']['data']['csrf']) && !$this->csrf->checkCsrf($request['request']['data']['csrf'])) {
                throw new UserException('Something totally wrong! CSRF protection!');
            }

            $controller = new $request['controller']($request['request']);
            $response = $controller->execute();

            $this->csrf->setCsrf();
            $this->renderer->sendOutput($response);
        } catch (UserException $userException) {
            $this->errorOutput(
                $userException,
                isset($request['request']['data']['method'])
                        && $request['request']['data']['method'] === 'ajax'
                    ? true : false
                );
        } catch (\Exception $exception) {
            //System exception. Must be logged
            echo $exception;
            die;
        }

    }

    protected function errorOutput($userException, $isAjax = false) {
        if (!$isAjax) {
            $_SESSION['error'] = $userException->getMessage();
            header('location:/');
            exit(1);
        }

        echo $userException->getMessage();
        exit(1);
    }

    /**
     * @return Renderer
     */
    public function getRenderer()
    {
        return $this->renderer;
    }

    /**
     * @param Renderer $renderer
     */
    public function setRenderer($renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * @return Router
     */
    public function getRouter()
    {
        return $this->router;
    }

    /**
     * @param Router $router
     */
    public function setRouter(Router $router)
    {
        $this->router = $router;
    }


}
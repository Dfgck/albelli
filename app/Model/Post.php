<?php
namespace Albelli\Model;

use Albelli\Core\AbstractModel;

class Post extends AbstractModel {

    public function getList()
    {
        $result = [];
        $data = $this->dbConnector->getList();

        foreach ($data as $row) {
            $result[] = new \Albelli\Model\Entity\Post($row);
        }

        return array_reverse($result);
    }

    public function save(array $data)
    {
        if (!empty($this->validators)) {
            $this->validate($data);
        }
        $this->dbConnector->save($data);
    }
}
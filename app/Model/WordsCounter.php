<?php
/**
 * Created by PhpStorm.
 * User: dfg
 * Date: 17.02.2018
 */

namespace Albelli\Model;


use Albelli\Core\AbstractModel;

class WordsCounter extends AbstractModel {

    public function getList()
    {
        //I must create WordEntity and use it
        $result = $this->dbConnector->getList();

        return !empty($result) ? $result[0] : [];
    }

    public function save(array $data)
    {
        $postWords = $this->cutShortWords(
            explode(' ', $this->textCleanup($data['post_body']))
        );

        $postWords = $this->calculate($postWords);

        $words = $this->getList();

        if (empty($words)) {
            $this->dbConnector->save($postWords);
            return true;
        }

        foreach ($postWords as $word => $count)
        {
            if (isset($words[$word])) {
                $words[$word] += $count;
            } else {
                $words[$word] = $count;
            }
        }

        arsort($words);
        $this->dbConnector->update([$words]);
    }

    protected function calculate($words)
    {
        $list = [];
        foreach ($words as $key => $value) {
            $list[$value] = isset($list[$value]) ? $list[$value] + 1 : 1;
        }

        arsort($list);
        return $list;
    }

    protected function textCleanup($text)
    {
        $text = str_replace([',', '.'], '', strtolower($text));
        $text = str_replace(PHP_EOL, ' ', $text);
        return $text;
    }

    protected function cutShortWords(array $words)
    {
        foreach ($words as $key => $value) {
            if (strlen($value) > 4) {
                continue;
            }

            unset($words[$key]);
        }
        return $words;
    }
}
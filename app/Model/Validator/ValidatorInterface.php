<?php
namespace Albelli\Model\Validator;


interface ValidatorInterface {

    /**
     * @param array $data
     * @return boolean
     */
    public function check(array $data);

}
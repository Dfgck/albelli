<?php
namespace Albelli\Model\Validator;


use Albelli\Core\Exception\UserException;

class AdminEmailValidator implements ValidatorInterface {

    protected $allowedEmails = ['test@test.com', 'foo@boo.com'];

    public function check(array $data)
    {
        if (in_array($data['post_email'], $this->allowedEmails)) {
            return true;
        }

        throw new UserException('Email not allowed');
    }
}
<?php
namespace Albelli\Model\Validator;

use Albelli\Core\Exception\UserException;

class EmailValidator implements ValidatorInterface {

    public function check(array $data)
    {
        if (filter_var($data['post_email'], FILTER_VALIDATE_EMAIL)) {
            return true;
        }

        throw new UserException('Incorrect Email');
    }
}
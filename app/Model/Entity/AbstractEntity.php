<?php
namespace Albelli\Model\Entity;

use Albelli\Core\EntityInterface;

abstract class AbstractEntity implements EntityInterface {

    protected $data;

    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function get($name)
    {
        if (!isset($this->data[$name])) {
            throw new \Exception("Unable find '$name' field");
        }

        return $this->data[$name];
    }

    public function set($name, $value)
    {
        $this->data[$name] = $value;
    }
}
function validatePostForm() {
    var result = true;

    $('.required').each(function(i, el) {

        if ($(el).val() == '') {
            showError($(el).attr('name')  + ' could not be empty!');
            result = false;
            return false;
        }

    });

    if ($('#postBody').val() == '' && $('#postImage').val() == '') {
        showError('Both the content and the Image fields could not be empty');
        result = false;
    }

    return result;
}
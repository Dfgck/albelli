// It will be better to use ReactJS
$(document).ready(function() {

    $('#addPostForm').submit(function (el) {
        el.preventDefault();
        removeAllMessages();

        if (!validatePostForm()) {
            return false;
        }

        var formData = new FormData();

        formData.append('method', 'ajax');
        formData.append('csrf', $('#csrf').val());
        formData.append('post_image', $('#postImage').get(0).files[0]);
        formData.append('post_title', $('#postTitle').val());
        formData.append('post_body', $('#postBody').val());
        formData.append('post_email', $('#postEmail').val());

        $.ajax({
            url: $(this).attr('action'),
            type:"POST",
            processData:false,
            contentType: false,
            data: formData,
            complete: function(data) {

                $('#content').load('/ #content > .blog-post');
                $('#mostUsedWords').load('/ #mostUsedWords > li');

                if (data.responseText != '') {
                    showError(data.responseText);
                } else {
                    $('#formBlock').load('/ #formBlock > form');
                    showSuccess('Post added!');
                }
            }
        });
    });

});

$(document).ajaxStart(function() {
    $('#savePostButton').append('<div id="loader" class="ball-scale-ripple" style="float: right;"><div></div></div>');
});

$(document).ajaxStop(function() {
    $('#loader').remove();
});

function showSuccess(message) {
    showBlock(getMessageBlock(message, 'success'));
}

function showError(message) {
    showBlock(getMessageBlock(message, 'error'));
}

function showBlock(block) {
    $(block).insertBefore('#formBlock');
}

function getMessageBlock(message, status) {
    return '<div data-alert="" class="alert-box ' + status +  '">'
        + message + '<a href="" class="close">×</a></div>';
}

function removeAllMessages() {
    $('.alert-box').remove();
}
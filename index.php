<?php
//It will be good idea to create Session wrapper
session_start();

define('ROOTPATH', __DIR__);

include __DIR__ . '/vendor/autoload.php';

$app = new \Albelli\App();
$app->setRouter(new \Albelli\Core\Router($_GET));
$app->setRenderer(new \Albelli\Core\Renderer());
$app->execute();
